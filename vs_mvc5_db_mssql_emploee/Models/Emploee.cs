﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vs_mvc5_db_mssql_emploee.Models
{
    public class Emploee
    {
        public int EmploeeId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public int PositionId { get; set; }
        public virtual Position Position { get; set; }
    }
}