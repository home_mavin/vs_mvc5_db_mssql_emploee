﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vs_mvc5_db_mssql_emploee.Models
{
    public class PageInfo
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalItem { get; set; }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((decimal)TotalItem / PageSize);
            }
        }
    }

    public class IndexViewModel
    {
        public IEnumerable<Emploee> Emploees { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}