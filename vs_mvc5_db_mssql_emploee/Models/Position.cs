﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vs_mvc5_db_mssql_emploee.Models
{
    public class Position
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public virtual ICollection<Emploee> Emploees { get; set; }
    }
}