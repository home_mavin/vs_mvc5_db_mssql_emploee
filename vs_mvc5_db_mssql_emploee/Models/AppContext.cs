﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace vs_mvc5_db_mssql_emploee.Models
{
    public class AppContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public AppContext() : base("name=AppContext")
        {
        }

        public System.Data.Entity.DbSet<vs_mvc5_db_mssql_emploee.Models.Emploee> Emploees { get; set; }

        public System.Data.Entity.DbSet<vs_mvc5_db_mssql_emploee.Models.Position> Positions { get; set; }
    }
}
