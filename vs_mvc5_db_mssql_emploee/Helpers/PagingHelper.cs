﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using vs_mvc5_db_mssql_emploee.Models;

namespace vs_mvc5_db_mssql_emploee.Helpers
{
    static public class PagingHelper
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html,
            PageInfo pageInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString PageLinks(this AjaxHelper html,
            PageInfo pageInfo, Func<int, string> pageUrl)
        {
            AjaxOptions ajaxOptions = new AjaxOptions();
            ajaxOptions.UpdateTargetId = "IndexPages";
            StringBuilder result = new StringBuilder();

            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");

                // Ajax теги
                tag.MergeAttribute("data-ajax", "true");
                tag.MergeAttribute("data-ajax-mode", "replace");
                tag.MergeAttribute("data-ajax-update", "#IndexPages");

                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString DeleteLinks(this AjaxHelper html,
            int EmploeeId, Func<int, string> pageUrl)
        {
            AjaxOptions ajaxOptions = new AjaxOptions();
            ajaxOptions.UpdateTargetId = "IndexPages";
            StringBuilder result = new StringBuilder();

            TagBuilder tag = new TagBuilder("a");

            // Ajax теги
            tag.MergeAttribute("data-ajax", "true");
            tag.MergeAttribute("data-ajax-mode", "replace");
            tag.MergeAttribute("data-ajax-update", "#IndexPages");

            tag.MergeAttribute("href", pageUrl(EmploeeId));
            tag.InnerHtml = "Delete";
            tag.AddCssClass("btn btn-default");
            result.Append(tag.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
    }
}