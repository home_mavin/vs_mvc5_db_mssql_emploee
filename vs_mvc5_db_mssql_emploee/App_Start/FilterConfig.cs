﻿using System.Web;
using System.Web.Mvc;

namespace vs_mvc5_db_mssql_emploee
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
