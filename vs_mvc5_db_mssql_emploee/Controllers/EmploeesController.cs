﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using vs_mvc5_db_mssql_emploee.Models;

namespace vs_mvc5_db_mssql_emploee.Controllers
{
    public class EmploeesController : Controller
    {
        private AppContext db = new AppContext();

        // GET: Emploees
        public ActionResult Index(int pageNum = 1)
        {
            //var emploees = db.Emploees.Include(e => e.Position);
            //return View(emploees.ToList());
            int pageSize = 5;
            IEnumerable<Emploee> emploesPerPages = db.Emploees
                .Include(e => e.Position)
                .OrderBy(x => x.EmploeeId)
                .Skip((pageNum - 1) * pageSize)
                .Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = pageNum, PageSize = pageSize, TotalItem = db.Emploees.Count() };
            IndexViewModel indexViewModel = new IndexViewModel { PageInfo = pageInfo, Emploees = emploesPerPages };
            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexPages", indexViewModel);
            }
            else
            {
                return View(indexViewModel);
            }
        }

        // GET: Emploees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emploee emploee = db.Emploees.Find(id);
            if (emploee == null)
            {
                return HttpNotFound();
            }
            return View(emploee);
        }

        // GET: Emploees/Create
        public ActionResult Create()
        {
            ViewBag.PositionId = new SelectList(db.Positions, "PositionId", "PositionName");
            return PartialView("Create");
        }

        // POST: Emploees/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmploeeId,LastName,FirstName,MiddleName,PositionId")] Emploee emploee)
        {
            if (ModelState.IsValid)
            {
                db.Emploees.Add(emploee);
                db.SaveChanges();
                return Json(new { success = true });
            }

            ViewBag.PositionId = new SelectList(db.Positions, "PositionId", "PositionName", emploee.PositionId);
            return Json(emploee, JsonRequestBehavior.AllowGet);
        }

        // GET: Emploees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emploee emploee = db.Emploees.Find(id);
            if (emploee == null)
            {
                return HttpNotFound();
            }
            ViewBag.PositionId = new SelectList(db.Positions, "PositionId", "PositionName", emploee.PositionId);
            return View(emploee);
        }

        // POST: Emploees/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmploeeId,LastName,FirstName,MiddleName,PositionId")] Emploee emploee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emploee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PositionId = new SelectList(db.Positions, "PositionId", "PositionName", emploee.PositionId);
            return View(emploee);
        }

        // GET: Emploees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emploee emploee = db.Emploees.Find(id);
            if (emploee == null)
            {
                return HttpNotFound();
            }
            return PartialView(emploee);
        }

        // POST: Emploees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            Emploee emploee = db.Emploees.Find(id);
            db.Emploees.Remove(emploee);
            db.SaveChanges();
            //return RedirectToAction("Index");
            //return Json(new { success = true });
            //return PartialView("IndexPages", indexViewModel);
            return Json(new { success = true });
            int pageNum = 1;
            ////var emploees = db.Emploees.Include(e => e.Position);
            ////return View(emploees.ToList());
            //int pageSize = 5;
            //IEnumerable<Emploee> emploesPerPages = db.Emploees
            //    .Include(e => e.Position)
            //    .OrderBy(x => x.EmploeeId)
            //    .Skip((pageNum - 1) * pageSize)
            //    .Take(pageSize).ToList();
            //PageInfo pageInfo = new PageInfo { PageNumber = pageNum, PageSize = pageSize, TotalItem = db.Emploees.Count() };
            //IndexViewModel indexViewModel = new IndexViewModel { PageInfo = pageInfo, Emploees = emploesPerPages };
            //return PartialView("IndexPages", indexViewModel);
            //if (Request.IsAjaxRequest())
            //{
            //    return PartialView("IndexPages", indexViewModel);
            //}
            //else
            //{
            //    return RedirectToAction("Index", indexViewModel);
            //}
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
