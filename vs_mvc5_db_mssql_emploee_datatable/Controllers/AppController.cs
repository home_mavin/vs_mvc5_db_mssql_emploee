﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace vs_mvc5_db_mssql_emploee_datatable.Controllers
{
    public class AppController : Controller
    {
        // GET: App
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult loaddata()
        {
            using (AppContextEntities dc = new AppContextEntities())
            {
                dc.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                var data = dc.Emploees.OrderBy(a => a.EmploeeId).ToList();
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}