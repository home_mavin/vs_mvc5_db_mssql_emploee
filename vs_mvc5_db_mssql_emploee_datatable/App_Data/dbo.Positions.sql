﻿CREATE TABLE [dbo].[Positions] (
    [PositionId]   INT            IDENTITY (1, 1) NOT NULL,
    [PositionName] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_dbo.Positions] PRIMARY KEY CLUSTERED ([PositionId] ASC)
);

