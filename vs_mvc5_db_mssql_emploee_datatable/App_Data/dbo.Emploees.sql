﻿CREATE TABLE [dbo].[Emploees] (
    [EmploeeId]  INT            IDENTITY (1, 1) NOT NULL,
    [LastName]   NVARCHAR(50) NOT NULL,
    [FirstName]  NVARCHAR(50) NOT NULL,
    [MiddleName] NVARCHAR(50) NOT NULL,
    [PositionId] INT            NOT NULL,
    CONSTRAINT [PK_dbo.Emploees] PRIMARY KEY CLUSTERED ([EmploeeId] ASC),
    CONSTRAINT [FK_dbo.Emploees_dbo.Positions_PositionId] FOREIGN KEY ([PositionId]) REFERENCES [dbo].[Positions] ([PositionId]) ON DELETE CASCADE,

);


GO
CREATE NONCLUSTERED INDEX [IX_PositionId]
    ON [dbo].[Emploees]([PositionId] ASC);

GO
ALTER TABLE [dbo].[Emploees]
ADD CONSTRAINT [AK_dbo.Emploees] UNIQUE([LastName], [FirstName], [MiddleName]);